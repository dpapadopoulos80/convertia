# convertia

A first realese of *convertia CE 0.5* is coming soon. Stay tuned for news at https://convertia.io!

## Develop

The framework is based on [JHipster](https://www.jhipster.tech/) and mostly shares the stack by using latest Angular as headless front and Spring Boot as the API backend.
